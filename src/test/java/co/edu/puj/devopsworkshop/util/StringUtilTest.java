package co.edu.puj.devopsworkshop.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringUtilTest {

    private String[] names = {"Julian", "Jair", "Michael", "Bryan"};

    @Test
    void concatWithDashTest() {
        String expected = "Julian-Jair-Michael-Bryan";

        String result = StringUtil.concatWithDash(names);

        assertEquals(expected, result);
    }

    @Test
    void concatWithUnderscoreTest() {
        String expected = "Julian_Jair_Michael_Bryan";

        String result = StringUtil.concatWithUnderscore(names);

        assertEquals(expected, result);
    }

}
