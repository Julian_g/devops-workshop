package co.edu.puj.devopsworkshop.util;

import java.util.Arrays;
import java.util.Collections;

public class StringUtil {

    private static String concatWith(String delimiter, String... params) {
        String result = String.join(delimiter, params);
        return result;
    }

    public static String concatWithDash(String... params) {
        //Arrays.sort(params);
        return concatWith("-", params);
    }

    public static String concatWithUnderscore(String... params) {
        return concatWith("_", params);
    }
}
